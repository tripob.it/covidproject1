const proxyMiddleware  = require('http-proxy-middleware');
process.env.NODE_ENV = 'production'
module.exports = {
  lintOnSave: false,
  //mode: 'production',
  devServer: {
    port:3000,
    proxy:{
          '/': {
              target: 'http://localhost:57572',
              changeOrigin: true,
              secure:false,
              pathRewrite: {'^/api': '/api'},
              logLevel: 'debug' 
          }
    }
    //proxy: 'http://localhost:57572/'
 
  }
};
