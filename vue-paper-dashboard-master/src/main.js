/*!

 =========================================================
 * Vue Paper Dashboard - v2.0.0
 =========================================================

 * Product Page: http://www.creative-tim.com/product/paper-dashboard
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE.md)

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */
import Vue from "vue";
import App from "./App";
import router from "./router/index";

import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VueLodash from 'vue-lodash'
import lodash from 'lodash'

import { ModalPlugin } from 'bootstrap-vue'
Vue.use(VueLodash, { name: 'custom' , lodash: lodash })
Vue.use(PaperDashboard);
Vue.use(HighchartsVue)
Vue.use(BootstrapVue)
Vue.use(ModalPlugin)
/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
